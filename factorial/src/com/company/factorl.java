
public class factorl {
    static long calculateFactorial(int n){
        long result = 1;
        for (long i = 1; i <=n; i ++){
            result = result*i;
        }
        return result;
    }

    public static void main(String[] args){
        System.out.println(calculateFactorial(20));
    }
}